import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.tasks import repeat_every
from starlette.background import BackgroundTasks
from starlette.responses import FileResponse

from youthamp import StorageService, LDAService, DataUpdateService, get_logger, InputUnitService, CsvConverter

origins = ["*"]

app = FastAPI(debug=True)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

logger = get_logger(__name__)

input_unit_service = InputUnitService()
storage_service = StorageService()
lda_service = LDAService(storage_service)
data_update_service = DataUpdateService(input_unit_service, storage_service)
csv_converter = CsvConverter(storage_service, lda_service)


@app.on_event("startup")
@repeat_every(seconds=60*60, wait_first=True)
def update_data() -> None:
    logger.info("Execute periodical job for updating data")
    try:
        stop_words = open('stopwords.txt', 'r').read().splitlines()
        data_update_service.update_twitter_data()
        data_update_service.update_instagram_data()
        data_update_service.update_lda_data(stop_words)

    except Exception as e:
        logger.error('Failed to update data')
        logger.exception(e)


@app.get('/document_stats')
async def get_twitter_document_stats():
    try:
        return storage_service.get_doc_stats()
    except Exception as e:
        logger.error('Failed to calculate document stats')
        logger.exception(e)
        return {}


@app.get('/topics/twitter/{method}/{num_topics}')
async def get_twitter_topics(method: str, num_topics: int):
    try:
        if method == 'lda':
            return lda_service.get_lda_model(storage_service.PLATFORM_TWITTER, num_topics)
        else:
            return {}
    except Exception as e:
        logger.error('Failed to retrieve lda model data for twitter')
        logger.exception(e)
        return {}


@app.get('/topics/instagram/{method}/{num_topics}')
async def get_instagram_topics(method: str, num_topics: int):
    try:
        if method == 'lda':
            return lda_service.get_lda_model(storage_service.PLATFORM_INSTAGRAM, num_topics)
        else:
            return {}
    except Exception as e:
        logger.error('Failed to retrieve lda model data for instagram')
        logger.exception(e)
        return {}


@app.get('/topics/mixed/{method}/{num_topics}')
async def get_mixed_topics(method: str, num_topics: int):
    try:
        if method == 'lda':
            return lda_service.get_lda_model(storage_service.PLATFORM_MIXED, num_topics)
        else:
            return {}
    except Exception as e:
        logger.error('Failed to retrieve lda model data for mixed data')
        logger.exception(e)
        return {}


def remove_file(path: str) -> None:
    os.unlink(path)


@app.get("/statistics/{platform}/{num_topics}")
async def download_statistics_file(platform: str, num_topics: int, background_tasks: BackgroundTasks):
    file_path = csv_converter.generate_csv_and_get_file_path(platform, num_topics)
    background_tasks.add_task(remove_file, file_path)
    return FileResponse(filename=file_path, path=file_path)


@app.get('/term-statistics/{term}/{topic_number}/{num_of_topics}/{platform}')
async def get_term_stats(term: str, topic_number: int, num_of_topics: int, platform: str):
    try:
        relevance = lda_service.get_relevance(term, topic_number, num_of_topics, platform)

        return {"sentimentStats": storage_service.get_term_stats(term),
                "term": term,
                "platforms": platform,
                "topic_number": topic_number,
                "relevance": relevance}

    except Exception as e:
        logger.error('Failed to retrieve term statistics')
        logger.exception(e)
        return {}
