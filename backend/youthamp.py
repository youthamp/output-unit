import os
import csv
import re
import logging.config
import json
import pyLDAvis.gensim_models as gs
import requests
from gensim import corpora
from pymongo import MongoClient, ReplaceOne
import gensim
import spacy
from spacy.tokenizer import _get_regex_pattern
import pandas as pd
import re
import logging
import base64
import configparser
import spacy.cli

spacy.cli.download("de_core_news_sm")


class StorageService:
    """ Service for persisting relevant data which is used for in the output-unit frontend"""
    MONGO_DB_CONNECTION_STRING = 'MONGO_DB_CONNECTION_STRING'
    PLATFORM_TWITTER = 'twitter'
    PLATFORM_INSTAGRAM = 'instagram'
    PLATFORM_MIXED = 'mixed'
    VALID_PLATFORMS = [PLATFORM_TWITTER, PLATFORM_INSTAGRAM, PLATFORM_MIXED]
    CONFIG_SECTION_OUTPUT_UNIT = 'OUTPUT_UNIT'
    CONFIG_KEY_DB_CONNECTION_STRING = 'mongo_db_connection_string'
    MONGO_DB_USERNAME = 'MONGO_DB_USERNAME'
    MONGO_DB_PASSWORD = 'MONGO_DB_PASSWORD'
    MONGO_DB_HOST = 'MONGO_DB_HOST'
    YOUTH_AMP_DB = 'youthamp'
    MONGO_DB_DOC_ID_TWEETS = 'tweets_id'
    TWEETS_COLLECTION = 'tweets'
    INSTAGRAM_TIMELINE_ITEM_COLLECTION = 'instagram_timeline_items'
    LDA_INPUT_COLLECTION = 'lda_input'
    LDA_MODEL_COLLECTION = 'lda_model'
    KEY_DATA = 'data'
    KEY_PLATFORM = 'platform'
    KEY_TOPICS = 'topics'

    def __init__(self):
        self.logger = get_logger(self.__class__.__name__)
        self.youth_amp_db = self.__get_youth_amp_db()

    def __get_youth_amp_db(self):
        try:
            return MongoClient(os.getenv(self.MONGO_DB_CONNECTION_STRING, self.__get_connection_string_localhost()))[
                self.YOUTH_AMP_DB]

        except Exception as e:
            self.logger.error('Failed to initialize mongoDB')
            self.logger.exception(e)
            exit(1)

    def __get_connection_string_localhost(self):
        """ Return connection string from the local config.ini file"""
        try:
            config_parser = configparser.ConfigParser()
            config_parser.read('config.ini')
            config = config_parser[self.CONFIG_SECTION_OUTPUT_UNIT]
            return 'mongodb://' + config[self.MONGO_DB_USERNAME] + ':' + config[
                self.MONGO_DB_PASSWORD] + '@' + config[self.MONGO_DB_HOST] + '/'
        except Exception as e:
            self.logger.error("Failed to read mongo db connection string from config.ini")
            self.logger.exception(e)
            return ''

    def get_term_stats(self, term: str):
        """ Get term statistics including: sentiment, number of occurrences etc."""
        try:
            if not term:
                raise Exception('Term stats can not be calculated: term is empty')

            regex = re.compile(term, re.IGNORECASE)
            tweets = list(self.youth_amp_db[self.TWEETS_COLLECTION].find({"message": regex}))
            timeline_items = list(self.youth_amp_db[self.INSTAGRAM_TIMELINE_ITEM_COLLECTION].find({"captionText": regex}))
            tweet_count = len(tweets) if tweets is not None else 0
            timeline_items_count = len(timeline_items) if timeline_items is not None else 0

            tweet_sentiments = [tweet['sentiment'] for tweet in tweets if tweet['sentiment'] is not None]
            timeline_item_sentiments = [timeline_item['sentiment'] for timeline_item in timeline_items if
                                        timeline_item['sentiment'] is not None]
            tweets_sentiment_avg = self.__get_sentiment_avg(tweet_sentiments)
            timeline_items_sentiment_avg = self.__get_sentiment_avg(timeline_item_sentiments)

            a = {"timeline_count": timeline_items_count,
                 "tweet_count": tweet_count,
                 "tweets_pos": self.__get_pos_sentiment_count(tweet_sentiments),
                 "tweets_neg": self.__get_neg_sentiment_count(tweet_sentiments),
                 "instagram_pos": self.__get_pos_sentiment_count(timeline_item_sentiments),
                 "instagram_neg": self.__get_neg_sentiment_count(timeline_item_sentiments),
                 "tweets_sentiment_avg": tweets_sentiment_avg,
                 "instagram_sentiment_avg": timeline_items_sentiment_avg}
            self.logger.info(a)
            return a

        except Exception as e:
            self.logger.info('Failed to calculate sentiment stats')
            self.logger.exception(e)
            return {"timeline_count": 'undefined',
                    "tweet_count": 'undefined',
                    "tweets_sentiment_avg": 'undefined',
                    "instagram_sentiment_avg": 'undefined'}

    def __get_neg_sentiment_count(self, list_of_sentiments):
        if len(list_of_sentiments) > 0:
            neg_count = sum([1 for sentiment in list_of_sentiments if sentiment['score'] < 0])
        else:
            neg_count = 0
        return neg_count

    def __get_pos_sentiment_count(self, list_of_sentiments):
        if len(list_of_sentiments) > 0:
            pos_count = sum([1 for sentiment in list_of_sentiments if sentiment['score'] >= 0])
        else:
            pos_count = 0
        return pos_count

    def __get_sentiment_avg(self, list_of_sentiments):
        if len(list_of_sentiments) > 0:
            magnitude_avg = sum([sentiment['magnitude'] for sentiment in list_of_sentiments]) / len(list_of_sentiments)
            score_avg = sum([sentiment['score'] for sentiment in list_of_sentiments]) / len(list_of_sentiments)
        else:
            magnitude_avg = 'undefined'
            score_avg = 'undefined'

        return {'magnitude_avg': magnitude_avg, 'score_avg': score_avg}

    def get_tweets(self):
        """ Get all currently persisted tweets"""
        try:
            return list(self.youth_amp_db[self.TWEETS_COLLECTION].find())
        except Exception as e:
            self.logger.debug('No tweets found in collection ' + self.TWEETS_COLLECTION)
            self.logger.exception(e)
            return list()

    def get_instagram_timeline_items(self):
        """ Get all currently persisted tweets"""
        try:
            return list(self.youth_amp_db[self.INSTAGRAM_TIMELINE_ITEM_COLLECTION].find())
        except Exception as e:
            self.logger.debug(
                'No instagram messages found in collection ' + self.INSTAGRAM_TIMELINE_ITEM_COLLECTION)
            self.logger.exception(e)
            return list()

    def get_lda_input(self, platform: str):
        """ Get the raw lda input (lemmatized texts) for the given platform"""

        if platform in self.VALID_PLATFORMS:
            return list(self.youth_amp_db[self.LDA_INPUT_COLLECTION]
                        .find_one({self.KEY_PLATFORM: platform})
                        .get(self.KEY_DATA))

    def get_lda_model(self, platform: str, num_of_topics: int):
        """ Return the LDA model corresponding to the given platform and the given number of topics"""
        return self.youth_amp_db[self.LDA_MODEL_COLLECTION].find_one(
            {self.KEY_PLATFORM: platform, self.KEY_TOPICS: num_of_topics}).get(self.KEY_DATA)

    def get_doc_stats(self):
        """ Get document stats like number of tweets etc. for twitter and instagram"""
        try:
            tweets_dataset = pd.DataFrame(self.get_tweets())

            if tweets_dataset.empty:
                twitter_num_users = 0
                twitter_date_min = 0
                twitter_date_max = ""
                twitter_num_docs = ""
            else:
                twitter_num_users = len(tweets_dataset['userId'].unique())
                twitter_date_min = tweets_dataset['createdAt'].min()
                twitter_date_max = tweets_dataset['createdAt'].max()
                twitter_num_docs = len(tweets_dataset)

            timeline_dataset = pd.DataFrame(self.get_instagram_timeline_items())

            if timeline_dataset.empty:
                timeline_num_users = 0
                timeline_date_min = 0
                timeline_date_max = ""
                timeline_num_docs = ""
            else:
                timeline_num_users = len(timeline_dataset['userId'].unique())
                timeline_date_min = timeline_dataset['createdAt'].min()
                timeline_date_max = timeline_dataset['createdAt'].max()
                timeline_num_docs = len(timeline_dataset)

            return {'twitterDocumentStats': {'numUsers': twitter_num_users, 'dateMin': twitter_date_min,
                                             'dateMax': twitter_date_max, 'numDocs': twitter_num_docs},
                    'instagramDocumentStats': {'numUsers': timeline_num_users, 'dateMin': timeline_date_min,
                                               'dateMax': timeline_date_max, 'numDocs': timeline_num_docs}}
        except Exception as e:
            self.logger.debug('Failed to calculate document stats')
            self.logger.exception(e)
            return {}

    def update_tweets(self, tweets_json):
        """ Insert new tweets. Only none existing items will be inserted. """
        try:
            update_requests = [ReplaceOne({'tweetId': tweet['tweetId']}, tweet, upsert=True) for tweet in tweets_json]
            if len(update_requests) > 0:
                bulk = self.youth_amp_db[self.TWEETS_COLLECTION].bulk_write(update_requests)
                self.logger.info("Bulk upsert result for tweets: " + str(bulk.bulk_api_result))
            else:
                self.logger.info('Did not receive any tweets from input-unit.')
        except Exception as e:
            self.logger.debug('Failed to insert tweets')
            self.logger.exception(e)

    def update_instagram_timeline_items(self, timeline_items_json):
        """ Insert new instagram timeline items. Only none existing items will be inserted. """
        try:
            update_requests = [
                ReplaceOne({'timelineItemId': timeline_item['timelineItemId']}, timeline_item, upsert=True) for
                timeline_item in timeline_items_json]
            if len(update_requests) > 0:
                bulk = self.youth_amp_db[self.INSTAGRAM_TIMELINE_ITEM_COLLECTION].bulk_write(update_requests)
                self.logger.info("Bulk upsert result for timeline items: " + str(bulk.bulk_api_result))
            else:
                self.logger.info('Did not receive any timeline items from input-unit.')
        except Exception as e:
            self.logger.debug('Failed to insert instagram timeline items')
            self.logger.exception(e)

    def delete_lda_input(self, platform):
        try:
            self.youth_amp_db[self.LDA_INPUT_COLLECTION].delete_many({self.KEY_PLATFORM: platform})
        except Exception as e:
            self.logger.error("Failed to delete data for platform " + platform)
            self.logger.exception(e)

    def update_lda_input(self, platform: str, texts_lemmatized):
        """ Update/override raw lda input (lemmatized texts) for the given platform (like twitter or instagram)"""

        if platform in self.VALID_PLATFORMS:
            self.youth_amp_db[self.LDA_INPUT_COLLECTION].delete_many({self.KEY_PLATFORM: platform})
            self.__insert_document(self.LDA_INPUT_COLLECTION, platform, texts_lemmatized)
        else:
            self.logger.debug('The given platform ' + platform + ' does not correspond to any collection.')

    def update_lda_model(self, platform: str, num_of_topics: int, data):
        """ Update/override the given lda model corresponding to the given platform and the number of topics"""
        if num_of_topics < 2:
            self.logger.debug('Topics must be an integer greater than 1')
            return

        if platform not in self.VALID_PLATFORMS:
            self.logger.debug('The given platform ' + platform + ' is invalid')
            return

        self.youth_amp_db[self.LDA_MODEL_COLLECTION].delete_many(
            {self.KEY_PLATFORM: platform, self.KEY_TOPICS: num_of_topics})
        document = {self.KEY_PLATFORM: platform, self.KEY_TOPICS: num_of_topics, self.KEY_DATA: data}
        self.youth_amp_db[self.LDA_MODEL_COLLECTION].insert_one(document)

    def lda_model_exists(self, platform: str, num_of_topics: int):
        """ Check if a LDA model with the given platform and number of given topics exists"""
        if num_of_topics < 2:
            self.logger.debug('Topics must be an integer greater than 1')
            return False

        if platform not in self.VALID_PLATFORMS:
            self.logger.debug('The given platform ' + platform + ' is invalid')
            return False

        return self.youth_amp_db[self.LDA_MODEL_COLLECTION].count_documents(
            {self.KEY_PLATFORM: platform, self.KEY_TOPICS: num_of_topics}) > 0

    def __insert_document(self, collection_name, platform: str, data):
        document = {self.KEY_PLATFORM: platform, self.KEY_DATA: data}
        self.youth_amp_db[collection_name].insert_one(document)


class DataUpdateService:

    def __init__(self, input_unit_service, storage_service):
        self.logger = get_logger(self.__class__.__name__)
        self.input_unit_service = input_unit_service
        self.storage_service = storage_service

    def update_twitter_data(self):
        """ Fetch twitter data from the input-unit backend, preprocess it and persist it in the local mongoDB"""
        try:
            tweets_json = self.input_unit_service.get_tweets()
            tweets_cleaned = [{'userId': tweet['userId'], 'tweetId': tweet['id'], 'createdAt': tweet['createdAt'],
                               'message': tweet['message'].replace('\n', ' '),
                               'sentiment': tweet['sentiment']} for tweet in tweets_json]
            self.storage_service.update_tweets(tweets_cleaned)
        except Exception as e:
            self.logger.debug('Failed to update twitter data')
            self.logger.exception(e)

    def update_instagram_data(self):
        """ Fetch instagram data from the input-unit backend, preprocess it and persist it in the local mongoDB"""
        try:

            timeline_items_json = self.input_unit_service.get_instagram_timeline_items()
            timeline_item_cleaned = [
                {'timelineItemId': timeline_item['id'], 'createdAt': timeline_item['captionCreatedOn'],
                 'userId': timeline_item['instagramUser']['id'],
                 'captionText': timeline_item['captionText'].replace('\n', ' '),
                 'sentiment': timeline_item['sentiment']} for
                timeline_item in timeline_items_json]
            self.storage_service.update_instagram_timeline_items(timeline_item_cleaned)
        except Exception as e:
            self.logger.debug('Failed to update instagram timeline item data')
            self.logger.exception(e)

    def update_lda_data(self, stop_words):
        """preprocess data needed for lda models and persist it in the local mongoDB"""
        tweets_data = pd.DataFrame(self.storage_service.get_tweets())
        timeline_items_data = pd.DataFrame(self.storage_service.get_instagram_timeline_items())
        self.__update_tweets_lda_data(tweets_data, stop_words)
        self.__update_timeline_lda_data(timeline_items_data, stop_words)
        self.__update_mixed_lda_data(tweets_data, timeline_items_data, stop_words)

    def __preprocess_timeline_data(self, timeline_items_data, stop_words):
        if not timeline_items_data.empty:
            try:
                timeline_items_data_clean = [re.sub(r'https?:\/\/[^\s]*', '', di, flags=re.MULTILINE) for di in
                                             timeline_items_data['captionText']]

                return [
                    [word for word in document.split() if word.lower() not in stop_words]
                    for document in timeline_items_data_clean
                ]

            except Exception as e:
                self.logger.error("Failed to preprocess timeline data")
                self.logger.exception(e)
                return list()

    def __preprocess_twitter_data(self, twitter_data, stop_words):
        if not twitter_data.empty:
            try:
                data_clean = [re.sub(r'https?:\/\/[^\s]*', '', di, flags=re.MULTILINE) for di in
                              twitter_data['message']]

                return [
                    [word for word in document.split() if word.lower() not in stop_words]
                    for document in data_clean
                ]

            except Exception as e:
                self.logger.error("Failed to preprocess twitter data")
                self.logger.exception(e)
                return list()

    def __update_tweets_lda_data(self, tweets_data, stop_words):
        if not tweets_data.empty:
            try:
                tweets = self.__preprocess_twitter_data(tweets_data, stop_words)
                if len(tweets) > 0:
                    tweets_lemmatized = self.__get_safe_lemmatize(tweets, stop_words)
                    self.storage_service.update_lda_input(StorageService.PLATFORM_TWITTER, tweets_lemmatized)

            except Exception as e:
                self.logger.error("Failed to update tweets lda data")
                self.logger.exception(e)
        else:
            self.logger.debug("Tweets data is empty. Lda data can not be updated")
            self.storage_service.delete_lda_input(StorageService.PLATFORM_TWITTER)

    def __update_timeline_lda_data(self, timeline_items_data, stop_words):
        if not timeline_items_data.empty:
            try:
                timeline_items = self.__preprocess_timeline_data(timeline_items_data, stop_words)
                if len(timeline_items) > 0:
                    timeline_items_lemmatized = self.__get_safe_lemmatize(timeline_items, stop_words)
                    self.storage_service.update_lda_input(StorageService.PLATFORM_INSTAGRAM, timeline_items_lemmatized)

            except Exception as e:
                self.logger.error("Failed to update timeline item lda data")
                self.logger.exception(e)
        else:
            self.logger.debug("Timeline data is empty. Lda data can not be updated")
            self.storage_service.delete_lda_input(StorageService.PLATFORM_INSTAGRAM)

    def __update_mixed_lda_data(self, tweets_data, timeline_items_data, stop_words):
        if tweets_data.empty and timeline_items_data.empty:
            self.logger.debug("Raw mixed data is empty. Can not update lda data")
            self.storage_service.delete_lda_input(StorageService.PLATFORM_MIXED)

        elif tweets_data.empty and not timeline_items_data.empty:
            try:
                timeline_items = self.__preprocess_timeline_data(timeline_items_data, stop_words)
                if len(timeline_items) > 0:
                    mixed_data = timeline_items
                    mixed_data_lemmatized = self.__get_safe_lemmatize(mixed_data, stop_words)
                    self.storage_service.update_lda_input(StorageService.PLATFORM_MIXED, mixed_data_lemmatized)

            except Exception as e:
                self.logger.error("Failed to update mixed lda data")
                self.logger.exception(e)

        elif not tweets_data.empty and timeline_items_data.empty:
            try:
                tweets = self.__preprocess_twitter_data(tweets_data, stop_words)

                if len(tweets) > 0:
                    mixed_data = tweets
                    mixed_data_lemmatized = self.__get_safe_lemmatize(mixed_data, stop_words)
                    self.storage_service.update_lda_input(StorageService.PLATFORM_MIXED, mixed_data_lemmatized)

            except Exception as e:
                self.logger.error("Failed to update mixed lda data")
                self.logger.exception(e)

        else:
            try:
                tweets = self.__preprocess_twitter_data(tweets_data, stop_words)
                timeline_items = self.__preprocess_timeline_data(timeline_items_data, stop_words)

                if len(tweets) > 0 and len(timeline_items) > 0:
                    mixed_data = tweets + timeline_items
                    mixed_data_lemmatized = self.__get_safe_lemmatize(mixed_data, stop_words)
                    self.storage_service.update_lda_input(StorageService.PLATFORM_MIXED, mixed_data_lemmatized)

            except Exception as e:
                self.logger.error("Failed to update mixed lda data")
                self.logger.exception(e)

    def __get_safe_lemmatize(self, items, stop_words):
        try:
            return list(self.__sent_to_words(self.__lemmatization(items), stop_words))
        except Exception as e:
            self.logger.error("Failed to get safe lemmatized data")
            self.logger.exception(e)
            return list()

    def __lemmatization(self, texts):
        """https://spacy.io/api/annotation"""

        allowed_postags = ['NOUN', 'ADJ', 'VERB', 'ADV']

        # load german model for spacy
        nlp = spacy.load('de_core_news_sm', disable=['parser', 'ner'])

        # get default pattern for tokens that don't get split
        re_token_match = _get_regex_pattern(nlp.Defaults.token_match)
        # add your patterns (here: hashtags and in-word hyphens)
        re_token_match = f"({re_token_match}|#\w+|\w+-\w+)"

        # overwrite token_match function of the tokenizer
        nlp.tokenizer.token_match = re.compile(re_token_match).match
        texts_out = []
        for sent in texts:
            doc = nlp(" ".join(sent))
            hashtags = [token.lemma_ for token in doc if token.lemma_[0] == '#']
            texts_out.append(
                (hashtags, " ".join([token.lemma_ if token.lemma_ not in ['-PRON-'] else '' for token in doc if
                                     token.lemma_[0] not in ['#', '@'] and token.pos_ in allowed_postags])))
        return texts_out

    def __sent_to_words(self, sentences, stop_words):
        for sentence in sentences:
            cleaned_text = gensim.utils.simple_preprocess(str(sentence[1]),
                                                          deacc=True)  # deacc=True removes punctuations
            if cleaned_text:
                yield [w for w in cleaned_text if w not in stop_words]


class LDAService:
    LDA_MODEL_COLLECTION = 'lda_model'
    KEY_DATA = 'data'
    KEY_PLATFORM = 'platform'
    KEY_TOPICS = 'topics'

    def __init__(self, storage_service):
        self.logger = get_logger(self.__class__.__name__)
        self.storage_service = storage_service

    def __get_lda_model_json(self, lda_model, num_of_documents):
        return {'topics_data': lda_model, 'num_documents': num_of_documents}

    def get_lda_model(self, platform: str, num_of_topics: int):
        texts_lemmatized = self.storage_service.get_lda_input(platform)
        id2word = corpora.Dictionary(texts_lemmatized)
        corpus = [id2word.doc2bow(text) for text in texts_lemmatized]

        if self.storage_service.lda_model_exists(platform, num_of_topics):
            lda_model = self.storage_service.get_lda_model(platform, num_of_topics)
            return self.__get_lda_model_json(lda_model, len(corpus))

        else:
            model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                    id2word=id2word,
                                                    num_topics=num_of_topics,
                                                    random_state=100,
                                                    update_every=1,
                                                    chunksize=100,
                                                    passes=10,
                                                    alpha='auto',
                                                    per_word_topics=True)

            vis = gs.prepare(model, corpus, id2word)
            lda_model = json.loads(vis.to_json())
            self.storage_service.update_lda_model(platform, num_of_topics, lda_model)
            return self.__get_lda_model_json(lda_model, len(corpus))

    def get_relevance(self, term, topic_number, num_of_topics, platform):
        try:
            if topic_number < 0:
                raise Exception('Term stats can not be calculated: topic_number must be positive')

            if num_of_topics < 0:
                raise Exception('Term stats can not be calculated: num_of_topics must be positive')

            if platform not in self.storage_service.VALID_PLATFORMS:
                raise Exception('Term stats can not be calculated: platform invalid')

            if platform == self.storage_service.PLATFORM_TWITTER:
                lda_model = self.get_lda_model(self.storage_service.PLATFORM_TWITTER, num_of_topics)

            elif platform == self.storage_service.PLATFORM_INSTAGRAM:
                lda_model = self.get_lda_model(self.storage_service.PLATFORM_INSTAGRAM, num_of_topics)

            else:
                lda_model = self.get_lda_model(self.storage_service.PLATFORM_MIXED, num_of_topics)

            terms = lda_model["topics_data"]["tinfo"]["Term"]
            freq = lda_model["topics_data"]["tinfo"]["Freq"]
            category = lda_model["topics_data"]["tinfo"]["Category"]

            indices = [index for index, element in enumerate(category) if element == 'Topic' + str(topic_number)]

            for index in indices:
                if terms[index] == term:
                    return freq[index]

        except Exception as e:
            self.logger.debug('Failed to retrieve relevance')
            self.logger.exception(e)
            return 'undefined'



class InputUnitService:
    """ Service for communicating with the input-unit backend"""

    CONFIG_SECTION_INPUT_UNIT = 'INPUT_UNIT'
    INPUT_UNIT_HOST_ENV = 'INPUT_UNIT_HOST'
    INPUT_UNIT_ADMIN_USERNAME_ENV = 'INPUT_UNIT_ADMIN_USERNAME'
    INPUT_UNIT_ADMIN_PASSWORD_ENV = 'INPUT_UNIT_ADMIN_PASSWORD'

    def __init__(self):
        self.logger = get_logger(self.__class__.__name__)
        try:
            self.__init_default_values()
            self.input_unit_host = os.getenv(self.INPUT_UNIT_HOST_ENV, self.default_host)
            self.input_unit_auth = self.__get_auth_key()

            if self.input_unit_host is None:
                raise Exception(self.INPUT_UNIT_HOST_ENV + ' not set.')

            self.input_unit_auth = self.__get_auth_key()
            if not self.input_unit_auth:
                raise Exception('input_unit_auth is None or empty.')

        except Exception as e:
            self.logger.debug('Failed read env variables')
            self.logger.exception(e)
            exit(1)

    def __init_default_values(self):
        try:
            config_parser = configparser.ConfigParser()
            config_parser.read('config.ini')
            config = config_parser[self.CONFIG_SECTION_INPUT_UNIT]
            self.default_host = config[self.INPUT_UNIT_HOST_ENV]
            self.default_input_unit_admin_username = config[self.INPUT_UNIT_ADMIN_USERNAME_ENV]
            self.default_input_unit_admin_password = config[self.INPUT_UNIT_ADMIN_PASSWORD_ENV]
        except Exception as e:
            self.logger.error('Failed to set default values for setting up communication with the input-unit')
            self.logger.exception(e)

    def __get_auth_key(self):
        input_unit_username = os.getenv(self.INPUT_UNIT_ADMIN_USERNAME_ENV, self.default_input_unit_admin_username)
        input_unit_password = os.getenv(self.INPUT_UNIT_ADMIN_PASSWORD_ENV, self.default_input_unit_admin_password)
        if input_unit_username is None:
            raise Exception('Env variable INPUT_UNIT_ADMIN_USERNAME not set.')

        if input_unit_password is None:
            raise Exception('Env variable INPUT_UNIT_ADMIN_PASSWORD not set.')

        auth_key_decoded = input_unit_username + ':' + input_unit_password
        return str(base64.b64encode(auth_key_decoded.encode("utf-8")), "utf-8")

    def __get_auth_token(self):
        try:
            token_request_url = 'http://' + self.input_unit_host + '/youthamp/input-unit/authentication/token'
            token_request_headers = {'Authorization': 'Basic {}'.format(self.input_unit_auth)}
            token_request = requests.get(token_request_url, headers=token_request_headers)
            self.logger.info("token_request")
            self.logger.info(token_request)
            token = token_request.json()['data']['access_token']
            return token
        except Exception as e:
            self.logger.debug('Failed to fetch auth token from input-unit')
            self.logger.exception(e)
            return None

    def get_tweets(self):
        try:
            tweets_url = 'http://' + self.input_unit_host + '/youthamp/input-unit/data/twitter' \
                                                            '/tweets?after=01.01.2000 '
            tweets_request_headers = {'Authorization': 'Bearer {}'.format(self.__get_auth_token())}
            tweets_request = requests.get(tweets_url, headers=tweets_request_headers)
            return tweets_request.json()

        except Exception as e:
            self.logger.debug('Failed to fetch data')
            self.logger.exception(e)

    def get_instagram_timeline_items(self):
        try:
            timeline_items_url = 'http://' + self.input_unit_host + '/youthamp/input-unit/data/instagram' \
                                                                    '/posts?after=01.01.2000 '
            timeline_items_headers = {'Authorization': 'Bearer {}'.format(self.__get_auth_token())}
            timeline_items_request = requests.get(timeline_items_url, headers=timeline_items_headers)
            return timeline_items_request.json()

        except Exception as e:
            self.logger.debug('Failed to fetch data')
            self.logger.exception(e)

    def get_sentiment_statistics(self, search_term):
        if not search_term:
            self.logger.debug('Can not fetch sentiment statistics data: search_term is empty!')
            return None
        try:
            sentiment_statistics_url = 'http://' + self.input_unit_host + '/youthamp/input-unit/data/sentiment/statistics?searchTerm=' + search_term
            sentiment_statistics_headers = {'Authorization': 'Bearer {}'.format(self.__get_auth_token())}
            sentiment_statistics_request = requests.get(sentiment_statistics_url, headers=sentiment_statistics_headers)
            return sentiment_statistics_request.json()
        except Exception as e:
            self.logger.debug('Failed to fetch sentiment statistics data')
            self.logger.exception(e)


class CsvConverter:

    def __init__(self, storage_service, lda_service):
        self.lda_service = lda_service
        self.logger = get_logger(self.__class__.__name__)
        self.storage_service = storage_service

    def generate_csv_and_get_file_path(self, platform, num_topics):
        if platform not in self.storage_service.VALID_PLATFORMS:
            raise Exception('unable to generate csv file: platform ' + str(platform) + ' is not a valid platform')

        if not isinstance(num_topics, int):
            raise Exception('unable to generate csv file: num_topics ' + str(num_topics) + ' is not a valid number')

        if platform == self.storage_service.PLATFORM_TWITTER:
            lda_model = self.lda_service.get_lda_model(self.storage_service.PLATFORM_TWITTER, num_topics)
        elif platform == self.storage_service.PLATFORM_INSTAGRAM:
            lda_model = self.lda_service.get_lda_model(self.storage_service.PLATFORM_INSTAGRAM, num_topics)
        else:
            lda_model = self.lda_service.get_lda_model(self.storage_service.PLATFORM_MIXED, num_topics)

        terms = lda_model["topics_data"]["tinfo"]["Term"]
        freq = lda_model["topics_data"]["tinfo"]["Freq"]
        category = lda_model["topics_data"]["tinfo"]["Category"]
        csv_file_path = 'data_' + platform + '_' + str(num_topics) + '.csv'
        try:

            with open(csv_file_path, 'w+', encoding='UTF8') as f:
                header = ['Thema', 'Schlagwort', 'Relevanz']
                writer = csv.writer(f)
                writer.writerow(header)

                for topic_number in range(1, num_topics + 1):
                    indices = [index for index, element in enumerate(category) if
                               element == 'Topic' + str(topic_number)]
                    for index in indices:
                        writer.writerow([topic_number, terms[index], freq[index]])
                f.close()
                return csv_file_path

        except Exception as e:
            self.logger.debug('Failed to create csv file')
            self.logger.exception(e)
            os.remove(csv_file_path)
            raise e


def get_logger(name):
    logger = logging.getLogger(name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s [%(name)-12s] %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger
