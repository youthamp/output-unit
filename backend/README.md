Python backend to do topic clustering
=====================================

The topic clustering backend is a RESTfull api created using FastAPI. 

# Install 

First you do need to install all the dependencies with:

```
$ pip install -r requirements.txt
```

# Run it

1. Download all the tweets with the script get_tweets.py. You do need to create first the file config.ini containing the authentication key for the user of the [input-unit backend](https://gitlab.com/youthamp/input-unit/-/blob/master/REST-API.md), as described in the section "Get jwt access token". After you created it run with:

```
$ python get_tweets.py
```

2. Proccess the tweets and create the LDA model with:

```
$ python process_texts.py
```

3. Start the REST API server with uvicorn:

```
$ uvicorn server:app
```
