import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  plugins: [vue()],
  publicDir: 'base',
  base: '/',
  build: {
    outDir: 'public',
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
    }
  }
})
