module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'xsm': '460px'
      },
      skew: {
        '45': '45deg'
      },
      padding: {
        '1/4': '25%'
      },
      colors: {
        main: {DEFAULT: '#07a6df'}
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
  ]
}
