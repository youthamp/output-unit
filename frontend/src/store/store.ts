import {createStore, useStore as baseUseStore, Store, MutationTree, ActionTree} from "vuex"
import {DATA_SOURCE_INSTAGRAM, METHOD_LDA, DATA_SOURCE_TWITTER} from "../types/types";

export type State = {
  colors: string[],
  visState: VisStateType,
  selectedOptions: SelectedOptions
}

export type VisStateType = {
  lambda: number
  topic: number
  term: string
}

export type SelectedOptions = {
  topics: number
  method: string
  dataSources: string[]
}

export const enum MUTATIONS {
  CHANGE_VIS_STATE = 'CHANGE_VIS_STATE',
  CHANGE_SELECTED_OPTIONS = 'CHANGE_SELECTED_OPTIONS'
}

const state: State = {
  colors: [
  "#60A5FA",
  "#818CF8",
  "#A78BFA",
  "#F472B6",
  "#F87171",
  "#FBBF24",
  "#34D399",
  ],
  visState: {
    lambda: 0.63,
    topic: 0,
    term: ""
  },
  selectedOptions: {
    topics: 4,
    method: METHOD_LDA,
    dataSources: [DATA_SOURCE_TWITTER, DATA_SOURCE_INSTAGRAM]
  }
}

const mutations: MutationTree<State> = {
  [MUTATIONS.CHANGE_VIS_STATE](state, newValue: VisStateType){
    state.visState = newValue
  },
  [MUTATIONS.CHANGE_SELECTED_OPTIONS](state, newValue: SelectedOptions){
    state.selectedOptions = newValue
  },
}

const store = createStore({
   state,
   mutations,
})

export default store
