export const DATA_SOURCE_TWITTER: string = 'twitter'
export const DATA_SOURCE_INSTAGRAM: string = 'instagram'
export const METHOD_LDA: string = 'LDA'

export interface TopicsData {
  mdsDat: {
    x: number[]
    y: number[]
    topics: number[]
    cluster: number[]
    Freq: number[]
  }
  tinfo: {
    Term: string[]
    Freq: number[]
    Total: number[]
    Category: string[]
    logprob: number[]
    loglift: number[]
  }
  'token.table': {
    Topic: string[]
    Freq: number[]
    Term: string[]
  }
  R: number
  'lambda.step': number
  'plot.opts': object
  'topic.order': number[]
}

export interface TopicTermsFrequency {
  [key: string]: TopicTerm[]
}

export interface TopicTerm {
  font?: string
  hasText?: boolean
  height?: number
  padding?: number
  rotate?: number
  size: number
  style?: string
  text: string
  weight?: string
  width?: number
  x?: number
  x0?: number
  x1?: number
  xoff?: number
  y?: number
  y0?: number
  y1?: number
}

export interface TwitterDocumentStats {
  dateMax: string
  dateMin: string
  numDocs: number
  numUsers: number
}

export interface InstagramDocumentStats {
  dateMax: string
  dateMin: string
  numDocs: number
  numUsers: number
}

export interface DocumentStats {
  twitterDocumentStats: TwitterDocumentStats
  instagramDocumentStats: InstagramDocumentStats
}

export interface SentimentStats {
  timeline_count: string
  tweet_count: string
  tweets_sentiment_avg: Sentiment
  instagram_sentiment_avg: Sentiment

}

export interface TermStats {
  sentimentStats: SentimentStats,
  term: string
  platforms: string
  topic_number: number
  relevance: string
}

export interface Sentiment {
  magnitude: string
  score: string
}

