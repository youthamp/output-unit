import { createWebHistory, createRouter } from "vue-router"
import Home from "../views/Home.vue"
import Display from "../views/Display.vue"
import Impressum from "../views/Impressum.vue"
import Uber from "../views/Uber.vue"
import TermStatistics from "../components/termStatistics.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/:method/:topics/:dataSource",
    name: "Display",
    component: Display
  },
  {
    path: "/impressum",
    name: "Impressum",
    component: Impressum,
  },
  {
    path: "/Uber",
    name: "Uber",
    component: Uber,
  },
  {
    path: "/term-statistics",
    name: "termStatistics",
    component: TermStatistics,
  }
]

const router = createRouter({
  history: createWebHistory('/'),
  routes,
})

router.beforeEach((to, from) => {
  window.scrollTo(0, 0)
})

export default router
